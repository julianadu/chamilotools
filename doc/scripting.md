# Scripting chamilotools

Here is a typical example of shell-script calling chamilotools:

```bash
#!/bin/bash

printf '%s' "Password: " >&2
read -s passwd
printf '\n' >&2

# Be verbose (not mandatory)
set -x

chamilotools delete-doc "${2-${1%.zip}}" --password-from-stdin <<EOF
$passwd
EOF

chamilotools upload-zip "$1" --password-from-stdin <<EOF
$passwd
EOF
```

Notes:

* We use bash's `read -s` to read the password interactively once, and
  pass it to chamilotools.
  
* Do non, **ever** use things like `echo "$passwd" | chamilotools ...`:
  if the password appears on the command-line, then
  anyone on the same machine can read it with e.g. `ps u`. This is why
  we use the `<<EOF ...` syntax. This requires `--password-from-stdin`
  because otherwise, chamilotools would try to access the terminal and
  prompt the user directly.
  
* This script uses `delete-doc`, which is a dangerous command. Make
  sure you have backups.
  
* This example does not specify `-u` and `-c`, hence you need to
  specify your username and course name in configuration files. See
  section "Configuration" in [README.md](../README.md).

For non-interactive scripts, an option is to hardcode the password in
the script (replace the first lines with
`passwd=<your-password-here>`. If you do so, make sure the file is
read-protected and that you trust any person with administrator
priviledge on the machines where the script is stored.
