#!/bin/bash

DIR=$(cd "$(dirname "$0")" && pwd)

if ! command -v readpass >/dev/null
then
    readpass () {
	printf '%s' "Password: " >&2
	read -s password
	printf '%s' "$password"
    }
fi

password=$(readpass)

set -e
set -x

for t in testSetGetIntro testCreateDownloadQuiz testUpload
do
    $DIR/$t.py --password-from-stdin \
		-u "$USER" -c "$COURSE" --instance "$INSTANCE" $OPTIONS <<EOF
$password
EOF
done
