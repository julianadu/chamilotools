# -*- coding: utf-8 -*-
#
# Copyright or © or Copr. Sébastien Viardot and Matthieu Moy (2016)
#
# Matthieu.Moy@grenoble-inp.fr, Sebastien.Viardot@grenoble-inp.fr
#
# This software is a computer program whose purpose is to interact with
# the Chamilo LMS from a client computer.
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import print_function

import mediawiki
import re
import urllib
import cgi
from chamilolib.ChamiloInstance import (
    MIMETEX,
    MATHJAX
)
from chamilolib.utilities import (
    toUnicode,
    encode,
)

from utilities import warn


def convertWikiImg(INSTANCE, desc, courseName):
    return re.sub(r"\[\[File:(.*?)\]\]",
                  r"<html><img src='%s/document/\g<1>'></html>" %
                  INSTANCE.get_url_course(courseName), desc)


def containsMath(desc):
    return re.search(r"<math>(.*?)</math>", desc, flags=re.M | re.DOTALL)


def convertMath(INSTANCE, desc, courseName):
    sout = ""
    while True:
        m = re.search(r"<math>(.*?)</math>", desc, flags=re.M | re.DOTALL)
        if not m:
            sout += convertWikiImg(INSTANCE, desc, courseName)
            break
        eq = m.group(1).strip()
        s = convertWikiImg(INSTANCE, desc[:m.start()], courseName)
        sout += s
        if INSTANCE.get_math_engine() == MIMETEX:
            sout += ('<html><img ' +
                     'src="' + INSTANCE.get_url_mimetex() + '?' +
                     urllib.quote(eq) + '" /></html>'
                     )
        elif INSTANCE.get_math_engine() == MATHJAX:
            sout += ('<span class="math-tex">`' +
                     cgi.escape(eq) +
                     '`</span>'
                     )
        else:
            raise ValueError('Invalid math engine %s.' % INSTANCE.get_math_engine())
        desc = desc[m.end():]
    return sout


def convert_desc(INSTANCE, s, syntax, q_type, field, courseName):
    if s is None:
        return None

    # Preprocess source
    if syntax is None or syntax == "html":
        return encode(s)

    if syntax != "mediawiki":
        if containsMath(s):
            warn("Your text contains `<math>`, the syntax should be mediawiki")
    elif syntax == "mediawiki":
        assert courseName
        s = convertMath(INSTANCE, s, courseName)
        s = mediawiki.wiki2html(s, False)
        s = s.replace('\n\n', '<br />')
    elif syntax == "pre" or syntax == "escape":
        if not s:
            return ""
        s = toUnicode(s)  # To allow proper HTML-encoding later
        s = cgi.escape(s)  # Encode HTML characters
        s = s.replace('\n', '<br />')
        # Encode non-ascii into &...;
        s = s.encode('ascii', 'xmlcharrefreplace')
    elif syntax.startswith("pygments:"):
        language = syntax.replace("pygments:", "")
        from pygments import highlight
        from pygments.lexers import get_lexer_by_name
        from pygments.formatters import HtmlFormatter

        lexer = get_lexer_by_name(language, stripall=True)
        formatter = HtmlFormatter(linenos=False, noclasses=True, cssclass="source")
        s = highlight(s, lexer, formatter)
    else:
        raise ValueError("Unsupported syntax %s" % syntax)

    if INSTANCE.has_double_escaping_bug():
        # Chamilo seems to automatically unescape what we give it, but
        # only in some cases.
        # Double-escape so that the end result is actually a
        # single-escaping.
        if q_type == "T" and field == "answer":
            s = cgi.escape(s)

    # Post-process (must come after escaping)
    if syntax == "pre":
        s = "<pre>%s</pre>" % s

    return encode(s)
